$(function(){
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover();
	$('.carousel').carousel({
		interval: 2000
	});
	$('#contacto').on('show.bs.modal', function (e){
		console.log('el modal se está mostrando');
		$('.contactoBtn').removeClass('btn-outline-primary');
		$('.contactoBtn').addClass('btn-primary');
		$('.contactoBtn').prop('disabled', true);
	});
	$('#contacto').on('shown.bs.modal', function (e){
		console.log('el modal se mostró');
	});
	$('#contacto').on('hide.bs.modal', function (e){
		console.log('el modal se está ocultando');
	});
	$('#contacto').on('hidden.bs.modal', function (e){
		console.log('el modal se ocultó');
		$('.contactoBtn').removeClass('btn-primary');
		$('.contactoBtn').addClass('btn-outline-primary');
		$('.contactoBtn').prop('disabled', false);
	});
});